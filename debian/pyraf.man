.TH PYRAF "1" "October 2021" "pyraf 2.2.0" "User Commands"
.SH NAME
pyraf \- Python interface for IRAF
.SH SYNOPSIS
.B pyraf
[\fI\,options\/\fR] [\fI\,savefile\/\fR]
.SH DESCRIPTION
PyRAF is a command language for running IRAF tasks that is based on
the Python scripting language. It gives users the ability to run IRAF
tasks in an environment that has all the power and flexibility of
Python. PyRAF can be installed along with an existing IRAF installation;
users can then choose to run either PyRAF or the IRAF CL.
.SH OPTIONS
.TP
\fB\-c\fR, \fB\-\-comand=\fR<cmd>
Command passed in as string (any valid PyRAF command)
.TP
\fB\-e\fR, \fB\-\-ecl\fR
Turn on ECL mode
.TP
\fB\-h\fR, \fB\-\-help\fR
Print help message
.TP
\fB\-i\fR, \fB\-\-no\-\-commandwrapper\fR
No command line wrapper, just run standard interactive Python shell
.TP
\fB\-m\fR, \fB\-\-commandwrapper\fR
Run command line wrapper to provide extra capabilities (default)
.TP
\fB\-n\fR, \fB\-\-nosplash\fR
No splash screen during startup (also see \fB\-x\fR)
.TP
\fB\-s\fR, \fB\-\-silent\fR
Silent initialization (does not print startup messages)
.TP
\fB\-V\fR, \fB\-\-version\fR
Print version info and exit
.TP
\fB\-v\fR, \fB\-\-verbose\fR
Set verbosity level (may be repeated to increase verbosity)
.TP
\fB\-x\fR, \fB\-\-nographics\fR
No graphics will be attempted/loaded during session
.TP
\fB\-y\fR, \fB\-\-ipython\fR
Run the IPython shell instead of the normal PyRAF command shell
.SH COPYRIGHT
Copyright \(co 2003 Association of Universities for Research in Astronomy
(AURA)
