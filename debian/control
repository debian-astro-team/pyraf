Source: pyraf
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>,
           Phil Wyett <philip.wyett@kathenas.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               iraf <!nocheck>,
               iraf-dev <!nocheck>,
               iraf-noao <!nocheck>,
               libx11-dev,
               python3-all-dev,
               python3-astropy <!nocheck>,
               python3-pytest <!nocheck>,
               python3-setuptools,
               python3-setuptools-scm,
               python3-tk
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-astro-team/pyraf
Vcs-Git: https://salsa.debian.org/debian-astro-team/pyraf.git
Homepage: https://iraf-community.github.io/pyraf.html
Rules-Requires-Root: no

Package: python3-pyraf
Architecture: any
Depends: iraf,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Recommends: python3-numpy,
            python3-tk
Suggests: ipython3,
          python3-matplotlib,
          python3-urwid
Provides: ${python3:Provides}
Description: Python interface for IRAF
 PyRAF is a command language for running IRAF tasks in a Python like
 environment. It works very similar to IRAF CL, but has been updated
 to allow such things as importing Python modules, GUI parameter
 editing and help. It can be imported into Python allowing you to run
 IRAF commands from within a larger script.
